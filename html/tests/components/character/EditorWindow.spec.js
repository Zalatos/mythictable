import { shallowMount, createLocalVue } from '@vue/test-utils';
import CharacterEditorWindow from '@/components/character/EditorWindow.vue';

import * as VueWindow from '@hscmap/vue-window';

const localVue = createLocalVue();
localVue.use(VueWindow);

describe('Editor Window', () => {
    it('should open', () => {
        const wrapper = shallowMount(CharacterEditorWindow, { localVue });
        wrapper.setData({ isOpen: true });
        expect(wrapper.vm.isOpen).toBeTruthy();
    });

    it('should cancel', () => {
        const wrapper = shallowMount(CharacterEditorWindow, { localVue });
        wrapper.setData({ isOpen: true });
        expect(wrapper.vm.isOpen).toBeTruthy();

        wrapper.setData({ character: { token: {} } });

        wrapper.vm.cancel();
        expect(wrapper.vm.isOpen).toBeFalsy();
        expect(wrapper.emitted().onCancel).toBeTruthy();
    });

    it('should set description', () => {
        const wrapper = shallowMount(CharacterEditorWindow, { localVue });
        wrapper.setData({ character: { attributes: { description: 'foo' } } });
        expect(wrapper.vm.description).toEqual('foo');
    });

    it('should not change character', () => {
        const wrapper = shallowMount(CharacterEditorWindow, { localVue });
        wrapper.setData({ character: { attributes: { name: 'foo' } } });
        wrapper.setData({ name: 'bar' });
        expect(wrapper.vm.character).toEqual({ attributes: { name: 'foo' } });
    });

    it('should change character on save', () => {
        const wrapper = shallowMount(CharacterEditorWindow, { localVue });
        wrapper.setData({ character: { attributes: { name: 'foo' } } });
        wrapper.setData({ name: 'bar' });
        wrapper.vm.save();
        expect(wrapper.vm.character).toEqual({ attributes: { name: 'bar' } });
    });

    it('should emit patch on save', () => {
        const wrapper = shallowMount(CharacterEditorWindow, { localVue });
        wrapper.setData({ character: { id: 'id01', attributes: { name: 'foo' } } });
        wrapper.setData({ name: 'bar' });
        wrapper.vm.save();
        expect(wrapper.emitted().onChange[0][0]).toEqual({
            characterId: 'id01',
            patch: [{ op: 'replace', path: '/attributes/name', value: 'bar' }],
        });
    });
});
