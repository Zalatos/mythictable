import { characters as defaultCharacters } from './defaultCharacterData';
import { addCharacter, getCharacters } from '@/core/api/files/files.js';

const LibraryStore = {
    namespaced: true,
    state: {
        characters: defaultCharacters,
    },
    mutations: {
        updateCharacters(state, characters) {
            state.characters = [...defaultCharacters, ...characters];
        },
    },
    actions: {
        async addCharacter({ dispatch }, event) {
            let files;
            files = event && event.target && event.target.files;
            await addCharacter(files)
                .then(() => {
                    dispatch('getCharacters');
                })
                .catch(err => {
                    console.error(err);
                });
        },
        async getCharacters({ commit }) {
            await getCharacters().then(result => {
                commit('updateCharacters', result);
            });
        },
    },
    getters: {
        characters: state => {
            return state.characters;
        },
    },
};

export default LibraryStore;
