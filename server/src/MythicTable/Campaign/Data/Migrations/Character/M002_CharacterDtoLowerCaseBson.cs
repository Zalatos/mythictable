﻿using Mongo.Migration.Migrations;
using MongoDB.Bson;

namespace MythicTable.Campaign.Data.Migrations.Character
{
    public class M002_CharacterDtoLowerCaseBson : Migration<CharacterDTO>
    {
        public M002_CharacterDtoLowerCaseBson()
            : base("0.0.2")
        {
        }

        public override void Up(BsonDocument document)
        {
            if (document.Contains("token"))
            {
                document["token"] = document["Token"].AsBsonDocument.Merge(document["token"].AsBsonDocument);
            }
            else
            {
                document["token"] = document["Token"];
            }
            document.Remove("Token");
            if (document.Contains("asset"))
            {
                document["asset"] = document["Asset"].AsBsonDocument.Merge(document["asset"].AsBsonDocument);
            }
            else
            {
                document["asset"] = document["Asset"];
            }
            document.Remove("Asset");
            document.Remove("Attributes");
        }

        public override void Down(BsonDocument document)
        {
        }
    }
}
